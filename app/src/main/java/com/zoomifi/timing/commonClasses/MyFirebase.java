package com.zoomifi.timing.commonClasses;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.zoomifi.timing.dataModels.InventoryItems;

import java.util.HashMap;

/**
 * Created by brst-pc93 on 8/3/17.
 */

public class MyFirebase
{
    public static MyFirebase instance = null;

    DatabaseReference root = FirebaseDatabase.getInstance().getReference().getRoot();


    public static MyFirebase getInstance()
    {
        if (instance == null)
        {
            instance = new MyFirebase();
        }

        return instance;
    }

    public void saveItem(InventoryItems inventoryItems)
    {

        HashMap<String, Object> result = new HashMap<>();
        result.put(MyConstants.ID, inventoryItems.getId());
        result.put(MyConstants.NAME,inventoryItems.getName() );
        result.put(MyConstants.ALTERNATENAME, inventoryItems.getAlternateName());
        result.put(MyConstants.CODE, inventoryItems.getCode());
        result.put(MyConstants.UNITNAME, inventoryItems.getUnitName());
        result.put(MyConstants.PRICE,inventoryItems.getPrice());
        result.put(MyConstants.PRICETYPE, inventoryItems.getPriceType());
        result.put(MyConstants.DEFAULT_TAX_RATES,inventoryItems.getDefaultTaxRates());
        result.put(MyConstants.SKU,inventoryItems.getSku());
        result.put(MyConstants.HIDDEN,inventoryItems.getHidden());
        result.put(MyConstants.IS_REVENUE,inventoryItems.getIsRevenue());
        result.put(MyConstants.SELECTED,inventoryItems.getIsActivated());
        result.put(MyConstants.MYPRICE, inventoryItems.getMyPrice());
        result.put(MyConstants.TIME_TYPE, inventoryItems.getTimeType());


        root.child(MyConstants.ITEMS).child(inventoryItems.getId()).updateChildren(result).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {

            }
        });
    }

    public Query getItems()
    {
        return root.child(MyConstants.ITEMS);
    }


   /* public Query checkMerchantID()
    {
        return root.child(MyConstants.MERCHANTS);
    }

    public void saveMerchantItem(String merchantID, String itemId)
    {
         root.child(MyConstants.MERCHANTS).child(merchantID).setValue(itemId);
    }*/

}
