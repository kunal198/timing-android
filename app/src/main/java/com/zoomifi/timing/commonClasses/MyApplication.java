package com.zoomifi.timing.commonClasses;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by brst-pc93 on 8/14/17.
 */

public class MyApplication extends Application
{
    @Override
    public void onCreate()
    {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
