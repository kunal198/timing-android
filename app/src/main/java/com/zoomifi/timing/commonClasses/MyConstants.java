package com.zoomifi.timing.commonClasses;

/**
 * Created by brst-pc93 on 8/3/17.
 */

public class MyConstants
{

    public static final String ITEMS="items";
    public static final String MERCHANTS="merchants";

    public static final String ID="id";
    public static final String NAME="name";
    public static final String ITEM_NAME="item_name";
    public static final String CATEGORY="category";
    public static final String TAX="tax";
    public static final String IS_CHECKED="is_checked";
    public static final String ALTERNATENAME="alternateName";
    public static final String CODE="code";
    public static final String ORDER_ID="order_id";
    public static final String ITEM_ID="item_id";
    public static final String UNITNAME="unitName";
    public static final String PRICE="price";
    public static final String IS_ACTIVE="is_active";
    public static final String PRICETYPE="priceType";
    public static final String DEFAULT_TAX_RATES="defaultTaxRates";
    public static final String SKU="sku";
    public static final String HIDDEN="hidden";
    public static final String IS_REVENUE="isRevenue";
    public static final String SELECTED="selected";
    public static final String MYPRICE="myprice";
    public static final String TIME_TYPE ="time_type";
    public static final String TIME="time";
    public static final String TIMER_ID="timer_id";
}
