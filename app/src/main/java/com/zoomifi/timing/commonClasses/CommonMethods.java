package com.zoomifi.timing.commonClasses;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.zoomifi.timing.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by brst-pc93 on 7/13/17.
 */

public class CommonMethods
{

    public Toast toast;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat simpleDateFormat_HHmm = new SimpleDateFormat("HH:mm", Locale.US);

    public static CommonMethods instance = new CommonMethods();

    public static CommonMethods getInstance()
    {
        return instance;
    }


    public void changeFragment(FragmentActivity activity, Fragment fragment, boolean haveToAddBackStack)
    {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        if (haveToAddBackStack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public String parseDateToddMMyyyy(String time)
    {

        String inputPattern = "yyyy-MM-dd HH:mm";
        String outputPattern = "dd MMM, yyyy @ h:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);

        String str = null;
        try
        {
            str = outputFormat.format(inputFormat.parse(time));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return str;
    }

    public String parseDateToddMMyyyy2(String date)
    {

        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);

        String str = null;
        try
        {
            str = outputFormat.format(inputFormat.parse(date));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return str;
    }


    public void hideKeyBoard(Activity activity)
    {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null)
        {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        }
    }


    public void hideKeyBoard(Context context,View view)
    {
        if (view != null)
        {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        }
    }


    public void showToast(Context context, String message)
    {
        if (toast != null)
        {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);

        toast.show();

    }

    public String getCurrentDateTime()
    {
        Calendar c = Calendar.getInstance();
        return simpleDateFormat.format(c.getTime());
    }


    public String[] getTimeDifference(String time)
    {
        int totalMinutes = 0;
        String showTime = null;
        try
        {
            Date startedDateTime = simpleDateFormat.parse(time);
            Date currentDateTime = simpleDateFormat.parse(getCurrentDateTime());

            long difference = currentDateTime.getTime() - startedDateTime.getTime();

            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

            totalMinutes = hours * 60 + min;

            String diff = hours + ":" + min;


            showTime = simpleDateFormat_HHmm.format(simpleDateFormat_HHmm.parse(diff));


        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return new String[]{showTime,"" + totalMinutes};

    }


    public String twoDigitsAfterDecimalWithoutRoundOff(double value)
    {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.FLOOR);

        return df.format(value);

    }

    public JSONObject createJsonObject(String id, String myPrice, String timeType)
    {
        JSONObject obj = new JSONObject();
        try
        {
            obj.put(MyConstants.ID, id);
            obj.put(MyConstants.TIME, CommonMethods.getInstance().getCurrentDateTime());
            obj.put(MyConstants.PRICE, myPrice);
            obj.put(MyConstants.TIME_TYPE, timeType);
            obj.put(MyConstants.IS_ACTIVE, "true");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return obj;
    }

}
