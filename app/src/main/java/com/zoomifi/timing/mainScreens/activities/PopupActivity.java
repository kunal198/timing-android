package com.zoomifi.timing.mainScreens.activities;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.v1.BindingException;
import com.clover.sdk.v1.ClientException;
import com.clover.sdk.v1.ServiceException;
import com.clover.sdk.v3.order.LineItem;
import com.clover.sdk.v3.order.Order;
import com.clover.sdk.v3.order.OrderConnector;
import com.zoomifi.timing.R;
import com.zoomifi.timing.commonClasses.CommonMethods;
import com.zoomifi.timing.commonClasses.MyConstants;
import com.zoomifi.timing.commonClasses.MyDialogs;
import com.zoomifi.timing.mainScreens.fragments.InventoryFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PopupActivity extends Activity
{
    String TAG = PopupActivity.class.getSimpleName();

    Context context;

    AlertDialog.Builder builder;

    private Account mAccount;
  //  private InventoryConnector mInventoryConnector;
    private OrderConnector orderConnector;


    String price;
    String itemID;
    String orderId;
    String timeType;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.activity_popup);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        context = this;

        int i = getIntent().getIntExtra(MyConstants.CODE, 0);
        String name = getIntent().getStringExtra(MyConstants.NAME);
        orderId = getIntent().getStringExtra(MyConstants.ORDER_ID);
        itemID = getIntent().getStringExtra(MyConstants.ITEM_ID);
        price = getIntent().getStringExtra(MyConstants.PRICE);
        timeType = getIntent().getStringExtra(MyConstants.TIME_TYPE);

        builder = new AlertDialog.Builder(context)
                  .setTitle(i == 1 ? "Timer Start Confirmation" : "Timer Stop Confirmation")
                  .setMessage(i == 1 ? "Do you want to start the timer for " + name + " ?" : "Do you want to stop the timer ?")
                  .setPositiveButton("YES", i == 1 ? onStartTimerClickListener : onStopTimerClickListener)
                  .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener()
                  {
                      public void onClick(DialogInterface dialog, int which)
                      {
                          dialog.dismiss();
                          PopupActivity.this.finish();
                      }
                  })
                  .setIcon(R.mipmap.ic_launcher)
        .setCancelable(false);


        AlertDialog alert = builder.create();
        // alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alert.show();


        startWork();

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        PopupActivity.this.finish();
    }

    private void startWork()
    {
        // Retrieve the Clover account
        if (mAccount == null)
        {
            mAccount = CloverAccount.getAccount(context);

            Log.e("Name", "" + mAccount.name);
//            Log.e(" KEY_MERCHANT_ID  ", "" +  mAccount.KEY_MERCHANT_ID);
//            Log.e(" CLOVER_AUTHTOKEN_TYPE  ", "" +  CloverAccount.CLOVER_AUTHTOKEN_TYPE);


            if (mAccount == null)
            {
                return;
            }
        }

        // Connect InventoryConnector and OrderConnector
      //  connect();


        orderConnector= InventoryFragment.orderConnector;
    }




    DialogInterface.OnClickListener onStartTimerClickListener = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
            dialogInterface.dismiss();

            PopupActivity.this.finish();

            new AddNotesAsyncTask(itemID, orderId, price).execute();
        }
    };


    DialogInterface.OnClickListener onStopTimerClickListener = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
            dialogInterface.dismiss();

           // PopupActivity.this.finish();

            new CalculatePaymentAsyncTask(orderId).execute();
        }
    };


    private class AddNotesAsyncTask extends AsyncTask<Void, Void, Void>
    {

        String itemIDLocal;
        String orderId;
        String myPrice;

        AddNotesAsyncTask(String itemID, String orderId, String myPrice)
        {
            this.itemIDLocal = itemID;
            this.orderId = orderId;
            this.myPrice = myPrice;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids)
        {
            try
            {
                String data = "";

                Order order = orderConnector.getOrder(orderId);

                /*if (order.hasNote())
                {
                    JSONArray jsonArray = new JSONArray(order.getNote());

                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        if (jsonObject.getString(MyConstants.ID).equals(id))
                        {
                            jsonArray.remove(i);
                        }
                    }
                    data = jsonArray.put(createJsonObject(id, myPrice)).toString();
                }
                else
                {
                    *//*JSONArray jsonArray = new JSONArray();
                    data = jsonArray.put(createJsonObject(id, myPrice)).toString();*//*

                }*/
                data = CommonMethods.getInstance().createJsonObject(itemIDLocal, myPrice,timeType).toString();

                order.setNote(data);

                orderConnector.updateOrder(order);
            }
            catch (RemoteException | ClientException | ServiceException | BindingException e)
            {
                e.printStackTrace();
            }
            /*catch (JSONException e)
            {
                e.printStackTrace();
            }*/

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);

            // new AddPricedItemAsyncTask(orderId).execute();
        }
    }


    private class CalculatePaymentAsyncTask extends AsyncTask<Void, Void, String>
    {
        String orderId;

//        String priceLocal;
//        String timeLocal;

        CalculatePaymentAsyncTask(String orderId)
        {
            this.orderId = orderId;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(Void... voids)
        {

            String data=null;
            try
            {
                Order order = orderConnector.getOrder(orderId);


                if (order.hasNote())
                {
                    String note = order.getNote();

                    /*JSONArray jsonArray = new JSONArray(note);

                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        price = jsonObject.getString(MyConstants.PRICE);
                        time = jsonObject.getString(MyConstants.TIME);
                    }*/


                    JSONObject jsonObject = new JSONObject(note);

//                    priceLocal = jsonObject.getString(MyConstants.PRICE);
//                    timeLocal = jsonObject.getString(MyConstants.TIME);


                    data=jsonObject.getString(MyConstants.PRICE)+","+jsonObject.getString(MyConstants.TIME)+","+jsonObject.getString(MyConstants.TIME_TYPE);
                }
            }
            catch (RemoteException | ClientException | ServiceException | BindingException e)
            {
                e.printStackTrace();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);

            String[] data=s.split(",");

            /*for (int i = 0; i <data.length ; i++)
            {
                Log.e(TAG, " "+i+" "+ data[i]);
            }*/


            String[] diff = CommonMethods.getInstance().getTimeDifference(data[1]);

            Log.e(TAG, "Time Diff " + diff);

            finalPriceEditText = MyDialogs.getInstance().paymentDialog(context, diff, data[0], data[2],submitClickListener);


        }
    }

    EditText finalPriceEditText;


    View.OnClickListener submitClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {


            PopupActivity.this.finish();

            String finalPrice = finalPriceEditText.getText().toString().trim();
            if (!finalPrice.isEmpty())
            {
                new AddPricedItemAsyncTask(orderId, finalPrice).execute();
            }

            InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(finalPriceEditText.getWindowToken(), 0);

            MyDialogs.getInstance().dialog.dismiss();

        }
    };

    private class AddPricedItemAsyncTask extends AsyncTask<Void, Void, Void>
    {
        String orderId;
        String itemID;
        String finalPrice;

        public AddPricedItemAsyncTask(String orderId, String finalPrice)
        {
            this.orderId = orderId;
            this.finalPrice = finalPrice;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {

            String data = "";
            try
            {
                //Log.e(TAG, "itemID  " + itemID);
                Log.e(TAG, "orderID  " + orderId);

                Order order = orderConnector.getOrder(orderId);

                try
                {
                    JSONObject jsonObject = new JSONObject(order.getNote());
                    jsonObject.put(MyConstants.IS_ACTIVE, "false");

                    itemID = jsonObject.getString(MyConstants.ID);

                    data = jsonObject.toString();
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                order.setNote(data);

                orderConnector.updateOrder(order);

                Double p = Double.parseDouble(finalPrice)*100;


                long pp=Math.round(p);

                //String timerId=MySharedPreference.getInstance().getTimerId(context);


                List<LineItem> lineItems = order.getLineItems();

                int pos=0;
                for (int i = 0; i <lineItems.size() ; i++)
                {
                    if(lineItems.get(i).getItem().getId().equals(itemID))
                    {
                       pos=i;
                        break;
                    }
                }
                List<String> lineItemsNew=new ArrayList<>();
                lineItemsNew.add(lineItems.get(pos).getId());

                orderConnector.deleteLineItems(orderId,lineItemsNew);



                LineItem lineItem = orderConnector.addVariablePriceLineItem(orderId, itemID, pp, null, null);
//                LineItem lineItem = orderConnector.addVariablePriceLineItem(orderId, timerId, pp, null, null);
                lineItem.setIsRevenue(false);


            }
            catch (RemoteException | ClientException | ServiceException | BindingException e)
            {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);

        }
    }


}
