package com.zoomifi.timing.commonClasses;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by brst-pc93 on 8/7/17.
 */

public class MySharedPreference
{
    private final String PreferenceName = "MyPreference";

    public MySharedPreference()
    {
    }

    private static MySharedPreference instance = null;

    public static MySharedPreference getInstance()
    {
        if (instance == null)
        {
            instance = new MySharedPreference();
        }
        return instance;
    }

    public SharedPreferences getPreference(Context context)
    {
        return context.getSharedPreferences(PreferenceName, Activity.MODE_PRIVATE);
    }

    public void saveActivatedIds(Context context, String id)
    {
        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putString(MyConstants.ID, id);
        editor.apply();
    }


    public String getActivatedIds(Context context)
    {
        return getPreference(context).getString(MyConstants.ID, "");
    }


   /* public void saveTimerId(Context context, String id)
    {
        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putString(MyConstants.TIMER_ID, id);
        editor.apply();
    }


    public String getTimerId(Context context)
    {
        return getPreference(context).getString(MyConstants.TIMER_ID, "");
    }*/
}
