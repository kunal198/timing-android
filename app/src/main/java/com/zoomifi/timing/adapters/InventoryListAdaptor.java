package com.zoomifi.timing.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.zoomifi.timing.R;
import com.zoomifi.timing.commonClasses.CommonMethods;
import com.zoomifi.timing.commonClasses.MyFirebase;
import com.zoomifi.timing.commonClasses.MySharedPreference;
import com.zoomifi.timing.dataModels.InventoryItems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by brst-pc93 on 8/2/17.
 */

public class InventoryListAdaptor extends RecyclerView.Adapter<InventoryListAdaptor.MyViewHolder>
{

    private String TAG = InventoryListAdaptor.class.getSimpleName();

    private Context context;

    private List<InventoryItems> inventoryList;

    int selectedPosition = -1;

    String[] priceTitlesArray ;//= context.getResources().getStringArray(R.array.rateTitles);

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView textViewName;
        EditText editText;
        CheckBox checkBox;
        Button buttonSave;
        Spinner spinnerPriceTitle;



        public MyViewHolder(View view)
        {
            super(view);

            textViewName = (TextView) view.findViewById(R.id.textViewName);
            editText = (EditText) view.findViewById(R.id.editText);
            buttonSave = (Button) view.findViewById(R.id.buttonSave);
            checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            spinnerPriceTitle = (Spinner) view.findViewById(R.id.spinnerPriceTitle);

            editText.setTag(editText.getKeyListener());
            editText.setKeyListener(null);

            editText.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (event.getAction() == MotionEvent.ACTION_UP)
                    {
                        editText.setKeyListener((KeyListener) editText.getTag());
                        enable(buttonSave);
                        selectedPosition=getAdapterPosition();
                    }
                    return false;
                }
            });


            buttonSave.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {


                    String price = editText.getText().toString().trim();
                    if (!price.isEmpty())
                    {
                        selectedPosition=-1;
                        disable(buttonSave);
                        editText.setKeyListener(null);
                        CommonMethods.getInstance().hideKeyBoard(context, editText);


                        inventoryList.get(getAdapterPosition()).setMyPrice(price);
                        inventoryList.get(getAdapterPosition()).setTimeType(spinnerPriceTitle.getSelectedItem().toString());
                        MyFirebase.getInstance().saveItem(inventoryList.get(getAdapterPosition()));
                    }
                    else
                    {
                        Toast.makeText(context,"Please enter price",Toast.LENGTH_SHORT).show();
                    }
                }
            });


            spinnerPriceTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                {
                    if(getUserInteraction())
                    {
                        String text = spinnerPriceTitle.getSelectedItem().toString();
                        Log.e(TAG, " Spinner Item " + text);
                        inventoryList.get(getAdapterPosition()).setTimeType(text);
                        enable(buttonSave);
                        selectedPosition=getAdapterPosition();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView)
                {

                }
            });

            spinnerPriceTitle.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent)
                {
                    userInteraction=true;
                    return false;
                }
            });


        }
    }

    public void enable(Button button)
    {
        button.setEnabled(true);
        button.setBackground(ContextCompat.getDrawable(context, R.drawable.selector_green));
    }

    public void disable(Button button)
    {
        button.setEnabled(false);
        button.setBackground(ContextCompat.getDrawable(context, R.drawable.selector_gray));
    }

    public InventoryListAdaptor(Context context, List<InventoryItems> inventoryList)
    {
        this.context = context;
        this.inventoryList = inventoryList;

        priceTitlesArray = context.getResources().getStringArray(R.array.rateTitles);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_inventory_list_item, parent, false);

        return new MyViewHolder(itemView);
    }
    boolean  userInteraction = false;

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {


        holder.textViewName.setText(inventoryList.get(position).getName());
        holder.editText.setText(inventoryList.get(position).getMyPrice());


        holder.checkBox.setOnCheckedChangeListener(null);
        holder.checkBox.setChecked(inventoryList.get(position).getIsActivated());

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if (!holder.editText.getText().toString().trim().isEmpty())
                {

                    holder.editText.setKeyListener(null);

                    inventoryList.get(position).setActivated(isChecked);
                    checkAndSave(inventoryList.get(position).getId());
                }
                else if (isChecked)
                {
                    CommonMethods.getInstance().showToast(context, "Please enter the price first");
                    holder.checkBox.setChecked(!isChecked);
                }
            }
        });


        if(inventoryList.get(position).getTimeType() != null)
        {
            String name=inventoryList.get(position).getTimeType();

            int selectedItemPosition = 0;

            for (int i = 0; i <priceTitlesArray.length; i++)
            {
                if(priceTitlesArray[i].equals(name))
                {
                    selectedItemPosition=i;
                }
            }

            Log.e(TAG,"Hello "+position+"  " +inventoryList.get(position).getTimeType());

            holder.spinnerPriceTitle.setSelection(selectedItemPosition);
        }
        else
        {
            holder.spinnerPriceTitle.setSelection(0);
        }

        userInteraction=false;



        if(selectedPosition == position)
        {
            enable(holder.buttonSave);
        }
        else
        {
            disable(holder.buttonSave);
        }

        Log.e(TAG, "" + position);
    }

    public boolean getUserInteraction()
    {
        return userInteraction;
    }



    public void checkAndSave(String id)
    {
        String ids = MySharedPreference.getInstance().getActivatedIds(context).trim();

        //Log.e(TAG,"Before IDS  "+ids);

        if (!ids.isEmpty())
        {
            List<String> idsList = new ArrayList<String>(Arrays.asList(ids.split(",")));

            if (!idsList.contains(id))
            {
                ids = ids + "," + id;

                MySharedPreference.getInstance().saveActivatedIds(context, ids);
            }
            else
            {
                idsList.remove(id);

                String listString = TextUtils.join(",", idsList);
                MySharedPreference.getInstance().saveActivatedIds(context, listString);

                // Log.e(TAG,"After IDS  "+listString);
            }
        }
        else
        {
            MySharedPreference.getInstance().saveActivatedIds(context, id);
        }

        //Log.e(TAG,"After IDS  "+ids);


    }

    public boolean isIdThere(String id)
    {
        String ids = MySharedPreference.getInstance().getActivatedIds(context).trim();
        if (!ids.isEmpty())
        {
            List<String> idsList = Arrays.asList(ids.split(","));

            if (idsList.contains(id))
            {
                return true;
            }
            return false;
        }
        else
        {
            return false;
        }
    }

    @Override
    public int getItemCount()
    {
        return inventoryList.size();
    }


    public void refresh(List<InventoryItems> inventorylist)
    {

        this.inventoryList = inventorylist;
        notifyDataSetChanged();
    }

    public void refreshFirebaseData(List<InventoryItems> firebaseInventorylist)
    {

        for (int i = 0; i < firebaseInventorylist.size(); i++)
        {

            for (int j = 0; j < inventoryList.size(); j++)
            {
                if (inventoryList.get(j).getId().equals(firebaseInventorylist.get(i).getId()))
                {
                    inventoryList.get(j).setMyPrice(firebaseInventorylist.get(i).getMyPrice());
                    inventoryList.get(j).setTimeType(firebaseInventorylist.get(i).getTimeType());

                    if (isIdThere(inventoryList.get(j).getId()))
                    {
                        inventoryList.get(j).setActivated(true);

                    }
                }
            }
        }

        notifyDataSetChanged();
    }


}