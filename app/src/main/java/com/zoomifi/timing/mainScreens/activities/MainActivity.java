package com.zoomifi.timing.mainScreens.activities;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.zoomifi.timing.R;
import com.zoomifi.timing.mainScreens.fragments.InventoryFragment;
import com.zoomifi.timing.commonClasses.CommonMethods;
import com.zoomifi.timing.commonClasses.MyReceiver;

public class MainActivity extends AppCompatActivity /*implements SearchView.OnQueryTextListener*/
{

    String TAG = MainActivity.class.getSimpleName();
    Context context;

    public Toolbar toolbar;

    public TextView textViewTitle;

    Fragment fragment = null;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        setUpViews();

        changeFragment(0, false);

        registerReciever();


    }

    @Override
    protected void onResume()
    {
        super.onResume();



    }

    MyReceiver receiver = new MyReceiver(this);

    public void registerReciever()
    {
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.clover.intent.action.LINE_ITEM_ADDED");
        filter.addAction("clover.intent.action.ACTIVE_REGISTER_ORDER");
/*        filter.addAction(Intents.ACTION_V1_ACTIVE_PAY_ORDER);


        filter.addAction(Intents.ACTION_CLOVER_PAY);
        filter.addAction(Intents.ACTION_ITEM_SELECT);
        filter.addAction(Intents.ACTION_PAYMENT_PROCESSED);
        filter.addAction(Intents.ACTION_PAY);
        filter.addAction(Intents.ACTION_START_APP_DETAIL);
        filter.addAction(Intents.ACTION_START_ORDER_MANAGE);
        filter.addAction(Intents.ACTION_V1_ORDER_BUILD_START);
        filter.addAction(Intents.ACTION_V1_ORDER_BUILD_STOP);

        filter.addAction(Intents.ACTION_ORDER_CREATED);
        filter.addAction(Intents.ACTION_LINE_ITEM_ADDED);
        filter.addAction(Intents.ACTION_ORDER_SAVED);
        filter.addAction(Intents.ACTION_ACTIVE_REGISTER_ORDER);
        filter.addAction(Intents.ACTION_V1_PAY_BUILD_SHOW);
        filter.addAction(Intents.ACTION_V1_PAY_BUILD_START);*/
        registerReceiver(receiver, filter);
    }

    public void myCallback(String itemId, String orderId)
    {
       // Log.e(TAG, "HELLO" + itemId);

        if(fragment instanceof InventoryFragment)
        {
            ((InventoryFragment) fragment).itemAddedByCloverApp(itemId,orderId);
        }
    }

    public void myCallback2(String orderId)
    {
        if(fragment instanceof InventoryFragment)
        {
            ((InventoryFragment) fragment).onSelected(orderId);
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        unRegisterReciever();
    }

    public void unRegisterReciever()
    {
        unregisterReceiver(receiver);
    }

    private void setUpViews()
    {


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       /* getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu);*/


        textViewTitle = (TextView) findViewById(R.id.textViewTitle);

    }

    public void changeFragment(int position, boolean addToBackStack)
    {
        CommonMethods.getInstance().hideKeyBoard(this);

        switch (position)
        {
            case 0:
                fragment = new InventoryFragment();

                break;

            case 1:
                //  fragment = new Messages();
                break;

            case 2:
                //  fragment = new MyBookings();
                break;


            case 3:
                // fragment = new Profile();
                break;

            case 4:
                // fragment = new History();


                break;


            default:
                break;
        }

        if (fragment != null)
        {
            CommonMethods.getInstance().changeFragment(this, fragment, addToBackStack);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.inventory_menu, menu);

        MenuItem searchMenuItem = menu.findItem(R.id.search);

//        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        EditText voiceIcon = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        voiceIcon.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        voiceIcon.setHint("Search Inventory Name");
        voiceIcon.setHintTextColor(ContextCompat.getColor(context, R.color.colorWhiteHint));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {

            @Override
            public boolean onQueryTextSubmit(String s)
            {
                // Log.e(TAG, "onQueryTextSubmit "+s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s)
            {
                // Log.e(TAG, "onQueryTextChange "+s);

                ((InventoryFragment) fragment).filterList(s);

                return false;
            }
        });

        /*MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Set styles for expanded state here
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                }
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Set styles for collapsed state here
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                }
                return true;
            }
        });*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.add:

                ((InventoryFragment) fragment).openAddItemDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



}