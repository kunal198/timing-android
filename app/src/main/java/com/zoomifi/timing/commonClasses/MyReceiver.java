package com.zoomifi.timing.commonClasses;

/**
 * Created by brst-pc93 on 8/4/17.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.clover.sdk.v1.Intents;

import com.zoomifi.timing.mainScreens.activities.MainActivity;

public class MyReceiver extends BroadcastReceiver
{

    String TAG = MyReceiver.class.getSimpleName();


    MainActivity mainActivity; //a reference to activity's context

    public MyReceiver()
    {
    }

    public MyReceiver(MainActivity maContext)
    {
        mainActivity = maContext;
    }


    @Override
    public void onReceive(Context context, Intent intent)
    {
        // Implement code here to be performed when
        // broadcast is detected

        String action = intent.getAction();

        Log.e(TAG, "HELLO ACTION " + action);

        if (action.equals(Intents.ACTION_LINE_ITEM_ADDED))
        {
            String itemId = intent.getStringExtra(Intents.EXTRA_CLOVER_ITEM_ID);
            String orderId = intent.getStringExtra(Intents.EXTRA_CLOVER_ORDER_ID);

            mainActivity.myCallback(itemId,orderId);

        }
        else if(action.equals(Intents.ACTION_ACTIVE_REGISTER_ORDER))
        {
            String orderId = intent.getStringExtra(Intents.EXTRA_ORDER_ID);
            //Log.e(TAG, "INORDER2 "+orderId);

            if(orderId != null)
            {
                mainActivity.myCallback2(orderId);
            }
        }
    }



}