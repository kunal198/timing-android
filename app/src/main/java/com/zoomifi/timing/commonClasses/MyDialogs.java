package com.zoomifi.timing.commonClasses;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.clover.sdk.v3.inventory.Category;
import com.clover.sdk.v3.inventory.Item;
import com.clover.sdk.v3.inventory.PriceType;
import com.clover.sdk.v3.inventory.TaxRate;
import com.zoomifi.timing.R;
import com.zoomifi.timing.mainScreens.fragments.InventoryFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by brst-pc93 on 8/9/17.
 */

public class MyDialogs
{

    String TAG = MyDialogs.class.getSimpleName();

    public Dialog dialog;

    public static MyDialogs instance = new MyDialogs();

    public static MyDialogs getInstance()
    {
        return instance;
    }

    public EditText paymentDialog(final Context context, String[] data, String price,String timeType, View.OnClickListener onClickListener)
    {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_payment);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.getWindow().setGravity(Gravity.TOP | Gravity.START);
        dialog.setCancelable(false);
        //dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.show();


        TextView textViewTitle = (TextView) dialog.findViewById(R.id.textViewTitle);
        textViewTitle.setText("Total Bill");

        TextView textViewHours = (TextView) dialog.findViewById(R.id.textViewHours);
        TextView textViewTimeType = (TextView) dialog.findViewById(R.id.textViewTimeType);
        textViewTimeType.setText("Price "+timeType);


        TextView textViewPrice = (TextView) dialog.findViewById(R.id.textViewPrice);

        final EditText editTextTotal = (EditText) dialog.findViewById(R.id.editTextTotal);

        if(price.trim().isEmpty())
        {
            price="0";
        }

        textViewHours.setText(data[0]); // data[0] is show time
        textViewPrice.setText("$ " + price);

        //Log.e(TAG,""+data[1]);


        double a;
        if(timeType.equals("Per Hour"))
        {
            a = Double.parseDouble(price) / 60;
        }
        else  if(timeType.equals("Per Minute"))
        {
            a = Double.parseDouble(price);
        }
        else // Per Day
        {
            a = Double.parseDouble(price) / (60*24);
        }

        double b = Double.parseDouble(data[1]); // data[1] is total minutes

        //Log.e(TAG,"a  "+a+"  b "+b);
        double total = a * b;

        //Log.e(TAG,"total "+total);

        editTextTotal.setText(CommonMethods.getInstance().twoDigitsAfterDecimalWithoutRoundOff(total));


        editTextTotal.setTag(editTextTotal.getKeyListener());

        editTextTotal.setKeyListener(null);

        Button buttonSubmit = (Button) dialog.findViewById(R.id.buttonSubmit);

        editTextTotal.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                editTextTotal.setKeyListener((KeyListener) editTextTotal.getTag());
                return false;
            }
        });


        buttonSubmit.setOnClickListener(onClickListener);

        return editTextTotal;


    }


    AlertDialog.Builder builder;

    public void showAlertDialog(Context context, String name, DialogInterface.OnClickListener onClickListener, int i)
    {
        builder = new AlertDialog.Builder(context)
                  .setTitle(i == 1 ? "Timer Start Confirmation" : "Timer Stop Confirmation")
                  .setMessage(i == 1 ? "Do you want to start the timer for " + name + " ?" : "Do you want to stop the timer ?")
                  .setPositiveButton("YES", onClickListener)
                  .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener()
                  {
                      public void onClick(DialogInterface dialog, int which)
                      {
                          dialog.dismiss();
                      }
                  })
                  .setIcon(R.mipmap.ic_launcher);


        AlertDialog alert = builder.create();
        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alert.show();
    }


    public void addItemDialog(final Context context, final InventoryFragment inventoryFragment, final List<TaxRate> taxRates, final List<Category> categories)
    {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_item);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.getWindow().setGravity(Gravity.TOP | Gravity.START);
        dialog.setCancelable(false);
        //dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.show();


        final EditText editTextItemName = (EditText) dialog.findViewById(R.id.editTextItemName);
        final Spinner spinnerCategory = (Spinner) dialog.findViewById(R.id.spinnerCategory);
        final Spinner spinnerTax = (Spinner) dialog.findViewById(R.id.spinnerTax);

        final CheckBox checkBox = (CheckBox) dialog.findViewById(R.id.checkBox);
        Button buttonAdd = (Button) dialog.findViewById(R.id.buttonAdd);

        String[] spinnerTaxArray = new String[taxRates.size()];
        //spinnerTaxArray[0]="None";
        Collections.reverse(taxRates);
        for (int i = 0; i < taxRates.size(); i++)
        {
            spinnerTaxArray[i] = taxRates.get(i).getName();
            Log.e(TAG, "TAX " + taxRates.get(i).getName() + "  " + taxRates.get(i).getRate());
        }

        ArrayAdapter<String> spinnerTaxArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, spinnerTaxArray);
        spinnerTaxArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTax.setAdapter(spinnerTaxArrayAdapter);


        String[] spinnerArray = new String[categories.size() + 1];
        spinnerArray[0] = "None";
        for (int i = 0; i < categories.size(); i++)
        {
            spinnerArray[i + 1] = categories.get(i).getName();
        }


        ArrayAdapter<String> spinnerCatArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, spinnerArray);
        spinnerCatArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(spinnerCatArrayAdapter);


        buttonAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String itemName = editTextItemName.getText().toString();
//                String category = editTextCategory.getText().toString();
//                String tax = editTextTax.getText().toString();

                Boolean isChecked = checkBox.isChecked();


                if (itemName.isEmpty())
                {
                    CommonMethods.getInstance().showToast(context, "Please enter the item name");
                }
                else
                {
                    /*HashMap<String, Object> result = new HashMap<>();
                    result.put(MyConstants.ITEM_NAME, itemName);
                    result.put(MyConstants.CATEGORY,spinnerCategory.getSelectedItem());
                    result.put(MyConstants.TAX,spinnerTax.getSelectedItem() );
                    result.put(MyConstants.IS_CHECKED,isChecked);*/

                    //MyFirebase.getInstance().checkMerchantItem(merchantKey,result);


                    Item item = new Item();
                    item.setName(itemName);
                    item.setIsRevenue(!isChecked);

                    if (spinnerCategory.getSelectedItemPosition() != 0)
                    {
                        List<Category> categoryFinal=new ArrayList<Category>();
                        categoryFinal.add(categories.get(spinnerCategory.getSelectedItemPosition()-1));

                        item.setCategories(categoryFinal);
                    }

                    List<TaxRate> taxFinal=new ArrayList<TaxRate>();
                    taxFinal.add(taxRates.get(spinnerTax.getSelectedItemPosition()));

                    item.setDefaultTaxRates(false);
                    item.setTaxRates(taxFinal);

                    Long price = Long.valueOf(0);
                    item.setPrice(price);
                    PriceType priceType = PriceType.VARIABLE;
                    item.setPriceType(priceType);


                    inventoryFragment.new GetAllInventoryItemsAsyncTask(item).execute();

                    dialog.dismiss();
                }

            }
        });




        /*TextView textViewTitle = (TextView) dialog.findViewById(R.id.textViewTitle);
        textViewTitle.setText("Total Bill");

        TextView textViewHours = (TextView) dialog.findViewById(R.id.textViewHours);
        TextView textViewPrice = (TextView) dialog.findViewById(R.id.textViewPrice);




        textViewHours.setText(data[0]);
        textViewPrice.setText("$ " + price);

        //Log.e(TAG,""+data[1]);
        double a = Double.parseDouble(price) / 60;
        double b = Double.parseDouble(data[1]);

        //Log.e(TAG,"a  "+a+"  b "+b);
        double total = a * b;

        //Log.e(TAG,"total "+total);

        editTextTotal.setText(CommonMethods.getInstance().twoDigitsAfterDecimalWithoutRoundOff(total));


        editTextTotal.setTag(editTextTotal.getKeyListener());

        editTextTotal.setKeyListener(null);

        Button buttonSubmit = (Button) dialog.findViewById(R.id.buttonSubmit);

        editTextTotal.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                editTextTotal.setKeyListener((KeyListener) editTextTotal.getTag());
                return false;
            }
        });



        buttonSubmit.setOnClickListener(onClickListener);

        return editTextTotal;*/


    }

}
