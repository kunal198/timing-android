package com.zoomifi.timing.dataModels;

/**
 * Created by brst-pc93 on 8/2/17.
 */


public class InventoryItems
{
    private String id;
    private String name;

    private String alternateName;
    private String code;
    private String unitName;
    private long price;
    private String priceType;
    private Boolean defaultTaxRates;
    private String sku;
    private Boolean hidden;
    private Boolean isRevenue;

    boolean isActivated;

    String myPrice;

    public void setMyPrice(String price)
    {
        this.myPrice = price;
    }

    public String getMyPrice()
    {
        return myPrice;
    }

    public boolean getIsActivated()
    {
        return isActivated;
    }

    public void setActivated(boolean activated)
    {
        this.isActivated = activated;
    }
    /*    private TaxRate taxRates;
    private ModifierGroups modifierGroups;
    private Tags tags;*/

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAlternateName()
    {
        return alternateName;
    }

    public void setAlternateName(String alternateName)
    {
        this.alternateName = alternateName;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getUnitName()
    {
        return unitName;
    }

    public void setUnitName(String unitName)
    {
        this.unitName = unitName;
    }

    public long getPrice()
    {
        return price;
    }

    public void setPrice(long price)
    {
        this.price = price;
    }

    public String getPriceType()
    {
        return priceType;
    }

    public void setPriceType(String priceType)
    {
        this.priceType = priceType;
    }

    public Boolean getDefaultTaxRates()
    {
        return defaultTaxRates;
    }

    public void setDefaultTaxRates(Boolean defaultTaxRates)
    {
        this.defaultTaxRates = defaultTaxRates;
    }

    public String getSku()
    {
        return sku;
    }

    public void setSku(String sku)
    {
        this.sku = sku;
    }

    public Boolean getHidden()
    {
        return hidden;
    }

    public void setHidden(Boolean hidden)
    {
        this.hidden = hidden;
    }

    public Boolean getIsRevenue()
    {
        return isRevenue;
    }

    public void setIsRevenue(Boolean isRevenue)
    {
        this.isRevenue = isRevenue;
    }

    String timeType;
    public String getTimeType()
    {
        return timeType;
    }

    public void setTimeType(String timeType)
    {
        this.timeType=timeType;
    }


   /* public TaxRate getTaxRates()
    {
        return taxRates;
    }

    public void setTaxRates(TaxRates taxRates)
    {
        this.taxRates = taxRates;
    }

    public ModifierGroups getModifierGroups()
    {
        return modifierGroups;
    }

    public void setModifierGroups(ModifierGroups modifierGroups)
    {
        this.modifierGroups = modifierGroups;
    }

    public Tags getTags()
    {
        return tags;
    }

    public void setTags(Tags tags)
    {
        this.tags = tags;
    }*/


    /*public class Tags
    {

        private List<Object> elements = null;

        public List<Object> getElements()
        {
            return elements;
        }

        public void setElements(List<Object> elements)
        {
            this.elements = elements;
        }

    }*/

   /* public class TaxRates
    {

        private List<Object> elements = null;

        public List<Object> getElements()
        {
            return elements;
        }

        public void setElements(List<Object> elements)
        {
            this.elements = elements;
        }

    }*/

   /* public class ModifierGroups
    {

        private List<Object> elements = null;

        public List<Object> getElements()
        {
            return elements;
        }

        public void setElements(List<Object> elements)
        {
            this.elements = elements;
        }

    }*/

}