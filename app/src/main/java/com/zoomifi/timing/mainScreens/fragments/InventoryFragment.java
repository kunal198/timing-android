package com.zoomifi.timing.mainScreens.fragments;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.v1.BindingException;
import com.clover.sdk.v1.ClientException;
import com.clover.sdk.v1.ServiceException;
import com.clover.sdk.v3.inventory.Category;
import com.clover.sdk.v3.inventory.InventoryConnector;
import com.clover.sdk.v3.inventory.Item;
import com.clover.sdk.v3.inventory.PriceType;
import com.clover.sdk.v3.inventory.TaxRate;
import com.clover.sdk.v3.order.Order;
import com.clover.sdk.v3.order.OrderConnector;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.zoomifi.timing.R;
import com.zoomifi.timing.adapters.InventoryListAdaptor;
import com.zoomifi.timing.commonClasses.MyConstants;
import com.zoomifi.timing.commonClasses.MyDialogs;
import com.zoomifi.timing.commonClasses.MyFirebase;
import com.zoomifi.timing.dataModels.InventoryItems;
import com.zoomifi.timing.mainScreens.activities.MainActivity;
import com.zoomifi.timing.mainScreens.activities.PopupActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by brst-pc93 on 8/2/17.
 */

public class InventoryFragment extends Fragment
{
    String TAG = InventoryFragment.class.getSimpleName();

    Context context;
    View view = null;

    RecyclerView recyclerViewInventory;

    public InventoryListAdaptor inventoryListAdaptor;


    private Account mAccount;
    private InventoryConnector mInventoryConnector;
    public static OrderConnector orderConnector;

    List<InventoryItems> inventoryList = new ArrayList<>();

    List<TaxRate> taxRates=null;
    List<Category> categories=null;



    public InventoryFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_inventory, container, false);

            context = getActivity();

            setUpViews();
        }
        return view;
    }


    private void setUpViews()
    {
        recyclerViewInventory = (RecyclerView) view.findViewById(R.id.recyclerViewInventory);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewInventory.getContext(), DividerItemDecoration.VERTICAL);
        recyclerViewInventory.addItemDecoration(dividerItemDecoration);


        inventoryListAdaptor = new InventoryListAdaptor(context, inventoryList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerViewInventory.setLayoutManager(mLayoutManager);
        recyclerViewInventory.setItemAnimator(new DefaultItemAnimator());
        recyclerViewInventory.setAdapter(inventoryListAdaptor);


        startWork();

    }

    private void startWork()
    {
        // Retrieve the Clover account
        if (mAccount == null)
        {
            mAccount = CloverAccount.getAccount(getActivity());

            Log.e("Name", "" + mAccount.name);
//            Log.e(" KEY_MERCHANT_ID  ", "" +  mAccount.KEY_MERCHANT_ID);
//            Log.e(" CLOVER_AUTHTOKEN_TYPE  ", "" +  CloverAccount.CLOVER_AUTHTOKEN_TYPE);


            if (mAccount == null)
            {
                return;
            }
        }

        // Connect InventoryConnector and OrderConnector
        connect();

        // Get Item
        new GetAllInventoryItemsAsyncTask().execute();
//        new CheckMerchantItemAsyncTask().execute();
    }

    public void refreshInventoryList()
    {
        inventoryListAdaptor.refresh(inventoryList);
    }

    public void filterList(String name)
    {
        List<InventoryItems> inventoryListLocal = new ArrayList<>();

        for (int i = 0; i < inventoryList.size(); i++)
        {
            if (inventoryList.get(i).getName().toLowerCase().contains(name.toLowerCase()))
            {
                inventoryListLocal.add(inventoryList.get(i));
            }
        }

        if (name.trim().length() > 0)
        {
            inventoryListAdaptor.refresh(inventoryListLocal);
        }
        else
        {
            refreshInventoryList();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        ((MainActivity) getActivity()).textViewTitle.setText("Inventory List");
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        disconnect();
    }

    private void connect()
    {
        disconnect();
        if (mAccount != null)
        {
            orderConnector = new OrderConnector(getActivity(), mAccount, null);
            orderConnector.connect();

            mInventoryConnector = new InventoryConnector(getActivity(), mAccount, null);
            mInventoryConnector.connect();
        }
    }


    private void disconnect()
    {
        if (orderConnector != null)
        {
            orderConnector.disconnect();
            orderConnector = null;
        }


        if (mInventoryConnector != null)
        {
            mInventoryConnector.disconnect();
            mInventoryConnector = null;
        }


    }



    public void openAddItemDialog()
    {
        MyDialogs.getInstance().addItemDialog(context,InventoryFragment.this,taxRates,categories);
    }


    public class GetAllInventoryItemsAsyncTask extends AsyncTask<Object, Object, List<InventoryItems>>
    {

        //  boolean isTimerItemCreated = false;

           Item item=null;
        //   String merchantKey;

        public GetAllInventoryItemsAsyncTask(Item item)
        {
            this.item=item;
        }

        public GetAllInventoryItemsAsyncTask()
        {
            // isTimerItemCreated=true;
        }


        @Override
        protected final List<InventoryItems> doInBackground(Object... params)
        {
            try
            {

                if(taxRates == null)
                {
                    taxRates = mInventoryConnector.getTaxRates();
                }

                if(categories == null)
                {
                    categories = mInventoryConnector.getCategories();
                }

                if(item != null)
                {
                    mInventoryConnector.createItem(item);
                }

                //Get inventory item
                List<InventoryItems> inventoryItemsList = new ArrayList<>();

                List<Item> items = mInventoryConnector.getItems();
                for (int i = 0; i < items.size(); i++)
                {
                      Log.e("Tag",""+items.get(i).getName());

                    if (items.get(i).getPriceType() == PriceType.VARIABLE)
                    {
                        Item item1 = items.get(i);
                        InventoryItems inventoryItems = new InventoryItems();
                        inventoryItems.setId(item1.getId());
                        inventoryItems.setName(item1.getName());
                        inventoryItems.setAlternateName(item1.getAlternateName());
                        inventoryItems.setCode(item1.getCode());
                        inventoryItems.setUnitName(item1.getUnitName());
                        inventoryItems.setPrice(item1.getPrice());
                        inventoryItems.setPriceType(item1.getPriceType().toString());
                        inventoryItems.setDefaultTaxRates(item1.getDefaultTaxRates());
                        inventoryItems.setSku(item1.getSku());
                        inventoryItems.setHidden(item1.getHidden());
                        inventoryItems.setIsRevenue(item1.getIsRevenue());
                        inventoryItems.setActivated(false);
                        inventoryItems.setMyPrice("");

                        inventoryItemsList.add(inventoryItems);
                    }
                }


                //   Log.e(TAG,"isTimerAvailable "+isTimerAvailable);

               /* if (!isTimerItemCreated)
                {
                    *//*Item i = new Item();
                    i.setName("Timer");
                    Long price = Long.valueOf(0);
                    i.setPrice(price);
                    PriceType priceT = PriceType.VARIABLE;
                    i.setPriceType(priceT);
                    Item finalTimer = mInventoryConnector.createItem(i);*//*

                    Item finalTimer = mInventoryConnector.createItem(item);



                    MyFirebase.getInstance().saveMerchantItem(merchantKey,finalTimer.getId());

                    MySharedPreference.getInstance().saveTimerId(context, finalTimer.getId());


                }*/

                // Log.e("Tag", "Timer ID " + MySharedPreference.getInstance().getTimerId(context));

                //return mInventoryConnector.getItems();
                return inventoryItemsList;

            }
            catch (RemoteException | ClientException | ServiceException | BindingException e)
            {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected final void onPostExecute(List<InventoryItems> inventoryItemsList)
        {
            if (inventoryItemsList.size() > 0)
            {
/*
                for (int i = 0; i < item.size(); i++)
                {
                    Item item1 = item.get(i);

                    InventoryItems inventoryItems = new InventoryItems();
                    inventoryItems.setId(item1.getId());
                    inventoryItems.setName(item1.getName());
                    inventoryItems.setAlternateName(item1.getAlternateName());
                    inventoryItems.setCode(item1.getCode());
                    inventoryItems.setUnitName(item1.getUnitName());
                    inventoryItems.setPrice(item1.getPrice());
                    inventoryItems.setPriceType(item1.getPriceType().toString());
                    inventoryItems.setDefaultTaxRates(item1.getDefaultTaxRates());
                    inventoryItems.setSku(item1.getSku());
                    inventoryItems.setHidden(item1.getHidden());
                    inventoryItems.setIsRevenue(item1.getIsRevenue());
                    inventoryItems.setActivated(false);
                    inventoryItems.setMyPrice("");

                    inventoryList.add(inventoryItems);
                }*/

                inventoryList = inventoryItemsList;

                refreshInventoryList();

                getFirebaseData();
            }
        }
    }


    private void getFirebaseData()
    {
        //   Log.e(TAG,"HELLO");

        Query query = MyFirebase.getInstance().getItems();

        query.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                ArrayList<InventoryItems> firebaseArrayList = new ArrayList<InventoryItems>();

                Iterator i = dataSnapshot.getChildren().iterator();
                while (i.hasNext())
                {
                    DataSnapshot dataSnapshot1 = (DataSnapshot) i.next();

                    //   Log.e(TAG,"dataSnapshot1 "+dataSnapshot1);
                    // Log.e(TAG,"CHILD "+dataSnapshot1.child(MyConstants.PRICE).getValue());

                    InventoryItems inventoryItems = new InventoryItems();
                    inventoryItems.setId((String) dataSnapshot1.child(MyConstants.ID).getValue());
                    inventoryItems.setMyPrice((String) dataSnapshot1.child(MyConstants.MYPRICE).getValue());
                    inventoryItems.setTimeType((String) dataSnapshot1.child(MyConstants.TIME_TYPE).getValue());
                    inventoryItems.setActivated((boolean) dataSnapshot1.child(MyConstants.SELECTED).getValue());

                    firebaseArrayList.add(inventoryItems);
                }
                inventoryListAdaptor.refreshFirebaseData(firebaseArrayList);

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

    }

  //  String price;
    String itemID;
    String orderId;

    public void itemAddedByCloverApp(final String itemID, final String orderId)
    {
        this.itemID = itemID;
        this.orderId = orderId;

        new CheckNotesAsyncTask(orderId, 1).execute();

        /*for (int i = 0; i < inventoryList.size(); i++)
        {
            if (inventoryList.get(i).getId().equals(itemID) && inventoryList.get(i).getIsActivated())
            {
                Log.e(TAG, "HELLO " + itemID);

                price = inventoryList.get(i).getMyPrice();

                MyDialogs.getInstance().showAlertDialog(context, inventoryList.get(i).getName(), onStartTimerClickListener, 1);
            }
        }*/

    }


    public void onSelected(String orderId)
    {
        this.orderId = orderId;

        new CheckNotesAsyncTask(orderId, 2).execute();

    }

    private class CheckNotesAsyncTask extends AsyncTask<Void, Void, Void>
    {

        String orderId;
        boolean haveNotes = false;
        int fromWhere = 0;

        CheckNotesAsyncTask(String orderId, int i)
        {
            this.orderId = orderId;
            fromWhere = i;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids)
        {
            try
            {
                Order order = orderConnector.getOrder(orderId);


                if (fromWhere == 2 && order.hasNote())
                {
                    JSONObject jsonObject = new JSONObject(order.getNote());

                    if (jsonObject.getString(MyConstants.IS_ACTIVE).equals("true"))
                    {
                        haveNotes = true;
                    }
                }
                else if (fromWhere == 1 && order.hasNote())
                {
                    haveNotes = true;
                }
            }
            catch (RemoteException | ClientException | ServiceException | BindingException e)
            {
                e.printStackTrace();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);

            if (fromWhere == 2 && haveNotes) // when we are going to stop the timer
            {
                // MyDialogs.getInstance().showAlertDialog(context, null, onStopTimerClickListener, 2);

                Intent intent = new Intent(context, PopupActivity.class);
                intent.putExtra(MyConstants.CODE, 2);
                intent.putExtra(MyConstants.NAME, "");
                intent.putExtra(MyConstants.ORDER_ID, orderId);
                intent.putExtra(MyConstants.ITEM_ID, itemID);
                intent.putExtra(MyConstants.PRICE, "");
                intent.putExtra(MyConstants.TIME_TYPE, "");
                startActivity(intent);
            }
            else if (fromWhere == 1 && !haveNotes) // When we are going to start the timer
            {
                for (int i = 0; i < inventoryList.size(); i++)
                {
                    // Checking the items from list which are activated
                    InventoryItems inventoryItems = inventoryList.get(i);

                    if (inventoryItems.getId().equals(itemID) && inventoryItems.getIsActivated())
                    {
//                        Log.e(TAG, "HELLO " + itemID);

                     //  String price = inventoryItems.getMyPrice();

                        Intent intent = new Intent(context, PopupActivity.class);
                        intent.putExtra(MyConstants.CODE, 1);
                        intent.putExtra(MyConstants.NAME, inventoryItems.getName());
                        intent.putExtra(MyConstants.ORDER_ID, orderId);
                        intent.putExtra(MyConstants.ITEM_ID, itemID);
                        intent.putExtra(MyConstants.PRICE, inventoryItems.getMyPrice());
                        intent.putExtra(MyConstants.TIME_TYPE, inventoryItems.getTimeType());
                        startActivity(intent);

                        break;

                        //MyDialogs.getInstance().showAlertDialog(context, inventoryList.get(i).getName(), onStartTimerClickListener, 1);
                    }
                }
            }
        }
    }



    /*boolean isMerchantItemAvailable;

    public class CheckMerchantItemAsyncTask extends AsyncTask<Void, Void, Void>
    {
         String merchantKey;
        List<TaxRate> taxRates;
        List<Category> categories;

        @Override
        protected Void doInBackground(Void... voids)
        {

            //Get Merchant id
            try
            {

                taxRates = mInventoryConnector.getTaxRates();
                categories = mInventoryConnector.getCategories();


                CloverAuth.AuthResult authResult = CloverAuth.authenticate(getActivity(), mAccount);

                merchantKey = authResult.merchantId;
                Log.e(TAG, "merchantId " + authResult.merchantId);

                Query query = MyFirebase.getInstance().checkMerchantID();

                isMerchantItemAvailable = false;

                query.addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        Iterator i = dataSnapshot.getChildren().iterator();
                        while (i.hasNext())
                        {
                            DataSnapshot dataSnapshot1 = (DataSnapshot) i.next();

                            String localKey = dataSnapshot1.getKey();
                            Log.e(TAG, "localKey " + localKey);
                            Log.e(TAG, "localValue " + dataSnapshot1.getValue().toString());

                            if (localKey.equals(merchantKey))
                            {
                                isMerchantItemAvailable = true;

                                MySharedPreference.getInstance().saveTimerId(context, dataSnapshot1.getValue().toString());

                                break;
                            }
                        }
                        Log.e(TAG, "isMerchantItemAvailable " + isMerchantItemAvailable);

                        if (!isMerchantItemAvailable)
                        {
                            // MyFirebase.getInstance().checkMerchantItem(merchantKey);\

                            MyDialogs.getInstance().addItemDialog(context,InventoryFragment.this,merchantKey,taxRates,categories);
                        }
                        else
                        {
                            new GetAllInventoryItemsAsyncTask().execute();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
            }
            catch (AuthenticatorException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (OperationCanceledException e)
            {
                e.printStackTrace();
            }
            catch (RemoteException e)
            {
                e.printStackTrace();
            }
            catch (ClientException e)
            {
                e.printStackTrace();
            }
            catch (ServiceException e)
            {
                e.printStackTrace();
            }
            catch (BindingException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
           // super.onPostExecute(aVoid);

        }
    }*/


/*    DialogInterface.OnClickListener onStartTimerClickListener = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
            dialogInterface.dismiss();

            new AddNotesAsyncTask(itemID, orderId, price).execute();
        }
    };

    DialogInterface.OnClickListener onStopTimerClickListener = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
            dialogInterface.dismiss();

            new CalculatePaymentAsyncTask(orderId).execute();
        }
    };


    private class AddNotesAsyncTask extends AsyncTask<Void, Void, Void>
    {

        String id;
        String orderId;
        String myPrice;

        AddNotesAsyncTask(String id, String orderId, String myPrice)
        {
            this.id = id;
            this.orderId = orderId;
            this.myPrice = myPrice;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids)
        {
            try
            {
                String data = "";

                Order order = orderConnector.getOrder(orderId);

                *//*if (order.hasNote())
                {
                    JSONArray jsonArray = new JSONArray(order.getNote());

                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        if (jsonObject.getString(MyConstants.ID).equals(id))
                        {
                            jsonArray.remove(i);
                        }
                    }
                    data = jsonArray.put(createJsonObject(id, myPrice)).toString();
                }
                else
                {
                    *//**//*JSONArray jsonArray = new JSONArray();
                    data = jsonArray.put(createJsonObject(id, myPrice)).toString();*//**//*

                }*//*
                data = CommonMethods.getInstance().createJsonObject(id, myPrice).toString();

                order.setNote(data);

                orderConnector.updateOrder(order);
            }
            catch (RemoteException | ClientException | ServiceException | BindingException e)
            {
                e.printStackTrace();
            }
            *//*catch (JSONException e)
            {
                e.printStackTrace();
            }*//*

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);

            // new AddPricedItemAsyncTask(orderId).execute();
        }
    }


    private class CalculatePaymentAsyncTask extends AsyncTask<Void, Void, Void>
    {
        String orderId;

        String price;
        String time;

        CalculatePaymentAsyncTask(String orderId)
        {
            this.orderId = orderId;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids)
        {
            try
            {
                Order order = orderConnector.getOrder(orderId);


                if (order.hasNote())
                {
                    String note = order.getNote();

                    *//*JSONArray jsonArray = new JSONArray(note);

                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        price = jsonObject.getString(MyConstants.PRICE);
                        time = jsonObject.getString(MyConstants.TIME);
                    }*//*


                    JSONObject jsonObject = new JSONObject(note);

                    price = jsonObject.getString(MyConstants.PRICE);
                    time = jsonObject.getString(MyConstants.TIME);

                }
            }
            catch (RemoteException | ClientException | ServiceException | BindingException e)
            {
                e.printStackTrace();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);


            String[] diff = CommonMethods.getInstance().getTimeDifference(time);

            Log.e(TAG, "Time Diff " + diff);

            finalPriceEditText = MyDialogs.getInstance().paymentDialog(context, diff, price, submitClickListener);


        }
    }

    EditText finalPriceEditText;


    View.OnClickListener submitClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {


            String finalPrice = finalPriceEditText.getText().toString().trim();
            if (!finalPrice.isEmpty())
            {
                new AddPricedItemAsyncTask(orderId, finalPrice).execute();
            }

            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(finalPriceEditText.getWindowToken(), 0);

            MyDialogs.getInstance().dialog.dismiss();

        }
    };


    private class AddPricedItemAsyncTask extends AsyncTask<Void, Void, Void>
    {
        String orderId;
        String itemID;
        String finalPrice;

        public AddPricedItemAsyncTask(String orderId, String finalPrice)
        {
            this.orderId = orderId;
            this.finalPrice = finalPrice;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {

            String data = "";
            try
            {
                //Log.e(TAG, "itemID  " + itemID);
                Log.e(TAG, "orderID  " + orderId);

                Order order = orderConnector.getOrder(orderId);

                try
                {
                    JSONObject jsonObject = new JSONObject(order.getNote());
                    jsonObject.put(MyConstants.IS_ACTIVE, "false");

                    itemID = jsonObject.getString(MyConstants.ID);

                    data = jsonObject.toString();
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                order.setNote(data);

                orderConnector.updateOrder(order);

                Double p = Double.parseDouble(finalPrice) * 100;


                long pp = Math.round(p);

                LineItem lineItem = orderConnector.addVariablePriceLineItem(orderId, itemID, pp, null, null);
                lineItem.setIsRevenue(false);


            }
            catch (RemoteException | ClientException | ServiceException | BindingException e)
            {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);

        }
    }*/


}
